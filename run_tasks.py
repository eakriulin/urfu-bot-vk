## -*- coding: utf-8 -*-
import arrow
from notifications import notifications


today = arrow.get(arrow.now().date())


def get_count_of_days(needed_day):
    if today.weekday() > needed_day:
        return 7 + needed_day - today.weekday()
    elif today.weekday() < needed_day:
        return needed_day - today.weekday() + 7
    else:
        return 7


notifications.check_subscription_first.apply_async(eta=today.replace(hours=18 - 5, days=get_count_of_days(0)))
notifications.check_subscription_second.apply_async(eta=today.replace(hours=18 - 5, days=get_count_of_days(2)))
notifications.check_subscription_third.apply_async(eta=today.replace(hours=18 - 5, days=get_count_of_days(4)))

#notifications.check_subscription_first.apply_async(eta=today.replace(hours=18 - 5, days=0))
#notifications.check_subscription_second.apply_async(eta=today.replace(hours=18 - 5, days=2))
#notifications.check_subscription_third.apply_async(eta=today.replace(hours=18 - 5, days=4))
