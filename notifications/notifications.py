import arrow

from vk_api.keyboard import VkKeyboardColor

from bot.settings import celery
from bot.strings import strings
from bot.api import ApiVk
from bot.repository.db import MongoDB


api_vk = ApiVk()
db = MongoDB()


@celery.task(name='check_subscription_first')
def check_subscription_first():
    if db.new_jobs.is_actual():
        send_vacancies(1)

    delay = arrow.get(arrow.now().date()).replace(hours=18 - 5, days=7)
    check_subscription_first.apply_async(eta=delay)


@celery.task(name='check_subscription_second')
def check_subscription_second():
    if db.new_jobs.is_actual():
        send_vacancies(2)

    delay = arrow.get(arrow.now().date()).replace(hours=18 - 5, days=7)
    check_subscription_second.apply_async(eta=delay)


@celery.task(name='check_subscription_third')
def check_subscription_third():
    if db.new_jobs.is_actual():
        send_vacancies(3)
        send_amount_of_new_vacancies()
        db.new_jobs.set_not_actual()

    delay = arrow.get(arrow.now().date()).replace(hours=18 - 5, days=7)
    check_subscription_third.apply_async(eta=delay)


def send_vacancies(subscription_number):
    users = db.users.repo.find({})
    for user in users:
        try:
            if len(user['subscriptions']) >= subscription_number:
                user['current_subscription'] = {
                    'search': user['subscriptions'][subscription_number-1]['search'],
                    'search_type': user['subscriptions'][subscription_number-1]['search_type']
                }
                db.users.set(user)
                show_new_vacancies(user)
        except Exception:
            pass


def show_new_vacancies(user):
    set_search_state(user, user['current_subscription']['search'][:-3],
                     user['current_subscription']['search_type'], 1)
    results = get_new_search_results(user)
    results_count = db.new_jobs.search_count(user['search_type'], user['search']) \
        if user['search_type'] != 'vacancy' else search_by_new_vacancies_count(user['search'])
    if results:
        send_new_results(user['user_id'], results, user['search'], results_count)


def set_search_state(user, search, search_type, search_page):
    user['search'] = search
    user['search_type'] = search_type
    user['search_page'] = search_page
    db.users.set(user)


def get_new_search_results(user):
    if user['search_type'] != 'vacancy':
        results = db.new_jobs.search(user['search_type'], user['search'], user['search_page'])
    else:
        results = search_by_new_vacancies(user['search'], user['search_page'])
    return results


def search_by_new_vacancies(vacancy, page):
    vacancies = []
    for row_vacancy in strings['new_vacancies']:
        if vacancy.lower() in row_vacancy.lower():
            vacancies.append(db.new_jobs.search('vacancy', row_vacancy, 1)[0])
    vacancies.reverse()
    return vacancies[(page-1)*5:page*5]


def search_by_new_vacancies_count(vacancy):
    vacancies = []
    for row_vacancy in strings['new_vacancies']:
        if vacancy.lower() in row_vacancy.lower():
            vacancies.append(db.new_jobs.search('vacancy', row_vacancy, 1)[0])
    return len(vacancies)


def send_new_results(user_id, results, search, amount):
    message = strings['subscription_header'].format(search, amount)
    for job in results:
        message += strings['vacancy'].format(job['vacancy'],
                                             job['salary'],
                                             job['responsibilities'],
                                             job['link'])
    keyboard = api_vk.create_keyboard([strings['btn_look_next'], VkKeyboardColor.DEFAULT],
                                      [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
    api_vk.send_message(user_id, message, keyboard)


def send_amount_of_new_vacancies():
    users = db.users.repo.find({'subscriptions': []})
    for user in users:
        amount = db.new_jobs.repo.find({}).count()
        message = strings['x_new_vacancies'].format(amount)
        api_vk.send_message(user['user_id'], message)