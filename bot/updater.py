## -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup

from bot.api import ApiVk
from bot.repository.db import MongoDB
from bot.strings import strings
from bot.settings import JOBS_URL


api_vk = ApiVk()
db = MongoDB()

class JobsUpdater:
    def _upload_job(self, job_title, job_salary, job_duties, job_contacts, job_sphere):
        salary = self._get_formatted_salary(job_salary)
        responsibilities = self._get_formatted_duties(job_duties)
        link = self._get_formatted_contacts(job_contacts)
        category = self._get_formatted_category(job_sphere)
        industry = self._get_formatted_industry(job_sphere)
        db.jobs.add(job_title, salary, responsibilities, link, category, industry)
        db.new_jobs.add(job_title, salary, responsibilities, link, category, industry)

    def _get_formatted_salary(self, job_salary):
        if job_salary:
            return strings['salary_format'] if job_salary == 'з/п не указана' else job_salary
        return ''

    def _get_formatted_duties(self, job_duties):
        if job_duties:
            text = job_duties.replace('- ', '– ')
            formatted_duties = '– ' + '\n– '.join(text.replace('\n', '').replace('\r', '').split('– ')[1:4])
            return formatted_duties if formatted_duties != '– ' else job_duties
        return ''

    def _get_formatted_contacts(self, job_contacts):
        if job_contacts:
            formatted_contacts = job_contacts.replace('Подробнее: ', '')
            try:
                return strings['contacts_format'].format(api_vk.get_short_link(formatted_contacts))
            except:
                return strings['contacts_format'].format(formatted_contacts)
        return ''

    def _get_formatted_category(self, job_sphere):
        if job_sphere:
            return strings['students_category'] if 'студенты' in job_sphere else strings['fulltime_category']
        return strings['fulltime_category']

    def _get_formatted_industry(self, job_sphere):
        industries = {
            'Автомобильный бизнес': 'Логистика и доставка',
            'Административный персонал': 'Административная работа',
            'Банки, инвестиции, лизинг': 'Финансы и банкинг',
            'Безопасность': 'Безопасность',
            'Бухгалтерия, управленческий учет, финансы предприятия': 'Учет, аудит, консалтинг',
            'Высший менеджмент': 'Менеджмент',
            'Государственная служба, некоммерческие организации': 'Госслужба',
            'Добыча сырья': 'Производство и добыча',
            'Домашний персонал': 'Прочее',
            'Закупки': 'Прочее',
            'Инсталляция и сервис': 'Прочее',
            'Информационные технологии, интернет, телеком': 'Работа в IT',
            'Искусство, развлечения, масс-медиа': 'Досуг, искусство, медиа',
            'Консультирование': 'Учет, аудит, консалтинг',
            'Маркетинг, реклама, PR': 'Маркетинг и PR',
            'Медицина, фармацевтика': 'Медицина и фармацевтика',
            'Наука, образование': 'Прочее',
            'Начало карьеры, студенты': 'Прочее',
            'Продажи': 'Продажи',
            'Производство': 'Производство и добыча',
            'Рабочий персонал': 'Прочее',
            'Спортивные клубы, фитнес, салоны красоты': 'Досуг, искусство, медиа',
            'Страхование': 'Финансы и банкинг',
            'Строительство, недвижимость': 'Работа в строительстве',
            'Транспорт, логистика': 'Логистика и доставка',
            'Туризм, гостиницы, рестораны': 'Досуг, искусство, медиа',
            'Управление персоналом, тренинги': 'Работа в HR',
            'Юристы': 'Юриспруденция'
        }
        if job_sphere:
            job_sphere = job_sphere.replace('Начало карьеры, студенты', '')
            for key in industries.keys():
                if key in job_sphere:
                    return industries[key]
        return 'Прочее'

    def _get_jobs_list(self):
        response = requests.get(JOBS_URL)
        html = BeautifulSoup(response.content, features='lxml')
        return html.find_all('tr')

    # Парсит HTML страницу с выгрузкой вакансий для чат-бота: https://времякарьеры.рф/forbot/
    # Загружает в MongoDB (jobs и new_jobs) вакансии, которых еще нет в боте.
    # job_title != 'Зарплата' используется для игнорирования хедера таблицы.
    def update_jobs(self):
        db.new_jobs.repo.remove({})
        html_jobs = self._get_jobs_list()
        for html_job in html_jobs:
            job_title = html_job.contents[1].string
            if job_title not in strings['vacancies'] and job_title != 'Зарплата':
                strings['vacancies'].append(job_title)
                job_salary = html_job.contents[3].string
                job_duties = html_job.contents[5].string
                job_contacts = html_job.contents[7].code.string
                job_sphere = html_job.contents[9].string
                self._upload_job(job_title, job_salary, job_duties, job_contacts, job_sphere)
