## -*- coding: utf-8 -*-
def create_job(vacancy, salary, responsibilities, link, category, industry):
    return {
        'vacancy': vacancy,
        'salary': salary,
        'responsibilities': responsibilities,
        'link': link,
        'category': category,
        'industry': industry
    }


class NewJobsRepository:
    def __init__(self, repo):
        self.repo = repo

    def search(self, key, value, page):
        results = list(self.repo.find({key: value}))
        results.reverse()
        return results[(page-1)*5:page*5]

    def search_count(self, key, value):
        return self.repo.find({key: value}).count()

    def add(self, vacancy, salary, responsibilities, link, category, industry):
        job = create_job(vacancy, salary, responsibilities, link, category, industry)
        self.repo.insert(job)
        return job

    def set_actual(self):
        self.repo.update_one({'doc_id': 'actuality'},
                             {"$set": {'is_actual': True, 'doc_id': 'actuality'}},
                             upsert=True)

    def set_not_actual(self):
        self.repo.update_one({'doc_id': 'actuality'},
                             {"$set": {'is_actual': False, 'doc_id': 'actuality'}},
                             upsert=True)

    def is_actual(self):
        return self.repo.find_one({'doc_id': 'actuality'})['is_actual']

    def remove_vacancy(self, vacancy):
        self.repo.delete_one({'vacancy': vacancy})
