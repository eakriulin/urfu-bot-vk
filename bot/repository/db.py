## -*- coding: utf-8 -*-
from pymongo import MongoClient

from bot.repository.user import UserRepository
from bot.repository.jobs import JobsRepository
from bot.repository.new_jobs import NewJobsRepository
import bot.settings as settings


class MongoDB:
    def __init__(self):
        client = MongoClient(host=settings.MONGO_HOST, port=settings.MONGO_PORT)
        db = client[settings.MONGO_DB]
        self.users = UserRepository(db.users)
        self.jobs = JobsRepository(db.jobs)
        self.new_jobs = NewJobsRepository(db.new_jobs)
