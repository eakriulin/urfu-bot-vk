## -*- coding: utf-8 -*-
def create_user(user_id):
    return {
        'user_id': user_id,
        'search': None,
        'search_type': None,
        'search_page': None,
        'current_subscription': None,
        'subscriptions': []
    }


class UserRepository:
    def __init__(self, repo):
        self.repo = repo

    def get(self, user_id):
        return self.repo.find_one({'user_id': user_id})

    def set(self, user):
        self.repo.update_one({'_id': user['_id']}, {"$set": user})

    def get_or_create(self, user_id):
        user = self.get(user_id)
        if user: return user

        return self.add(user_id)

    def exists(self, user_id):
        return self.repo.find_one({'user_id': user_id})

    def add(self, user_id):
        user = create_user(user_id)
        self.repo.insert(user)
        return user
