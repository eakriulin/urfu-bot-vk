## -*- coding: utf-8 -*-
import vk_api
from random import randrange
from vk_api.keyboard import VkKeyboard

from bot.settings import token
#from bot.settings import token_test as token # testing stuff

INT_64_MAX_VALUE = 9223372036854775808

class ApiVk:
    def __init__(self):
        self.session = vk_api.VkApi(token=token)
        self.vk = self.session.get_api()

    def send_message(self, user_id, message, keyboard=None):
        self.vk.messages.send(
            user_id=user_id,
            peer_id=user_id,
            message=message,
            keyboard=keyboard,
            random_id=randrange(0, INT_64_MAX_VALUE)
        )

    def send_chat_action(self, user_id):
        self.vk.messages.setActivity(
            user_id=user_id,
            peer_id=user_id,
            type='typing'
        )

    def create_keyboard(self, *args, columns=1):
        keyboard = VkKeyboard()
        i = 0
        while i < len(args):
            keyboard.add_button(args[i][0], color=args[i][1])
            if args[i] in args[columns-1::columns] and args[i] != args[-1]:
                keyboard.add_line()
            i += 1
        return keyboard.get_keyboard()

    def get_user_info(self, user_id):
        return self.vk.users.get(user_ids=user_id, fields=['contacts'])

    def get_short_link(self, url):
        return self.vk.utils.getShortLink(url=url)['short_url']

