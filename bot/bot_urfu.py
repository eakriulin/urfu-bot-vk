## -*- coding: utf-8 -*-
from vk_api.keyboard import VkKeyboardColor

from bot.strings import strings
from bot.api import ApiVk
from bot.repository.db import MongoDB
from bot.analytics import Analytics
from bot.updater import JobsUpdater


api_vk = ApiVk()
db = MongoDB()
updater = JobsUpdater()


class BotUrfu:
    def login(self, user_id):
        api_vk.send_chat_action(user_id)
        db.users.get_or_create(user_id)
        message = strings['hello']
        keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                          [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)
        Analytics.pageview(user_id, 'login')

    def choose_search_type(self, user_id):
        message = strings['choose_search_type']
        keyboard = api_vk.create_keyboard([strings['btn_for_students'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_by_industries'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)
        Analytics.pageview(user_id, 'search_start')

    def job_for_students(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, strings['btn_for_students'], 'category', 1)
        results = db.jobs.search('category', strings['btn_for_students'], user['search_page'])
        results_count = db.jobs.search_count('category', strings['btn_for_students'])
        self.send_results(user_id, results, strings['btn_for_students'], results_count)
        Analytics.pageview(user_id, 'search_for_students')

    def show_industries(self, user_id):
        api_vk.send_chat_action(user_id)
        db.users.get_or_create(user_id)
        message = strings['what_sphere']

        buttons = db.jobs.get_industries()[:]
        buttons.append(strings['btn_main_menu'])
        for i in range(len(buttons)):
            if buttons[i] != buttons[-1]:
                buttons[i] = [buttons[i], VkKeyboardColor.DEFAULT]
            else:
                buttons[i] = [buttons[i], VkKeyboardColor.PRIMARY]
        keyboard = api_vk.create_keyboard(*buttons, columns=2)
        api_vk.send_message(user_id, message, keyboard)
        Analytics.pageview(user_id, 'search_by_industries')

    def job_by_industries(self, user_id, industry):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, industry, 'industry', 1)
        results = db.jobs.search('industry', industry, user['search_page'])
        results_count = db.jobs.search_count('industry', industry)
        self.send_results(user_id, results, industry, results_count)
        Analytics.pageview(user_id, 'search_by_industries_{}'.format(industry.lower().replace(' ', '_')))

    def job_by_vacancies_or_subscription(self, user_id, message):
        api_vk.send_chat_action(user_id)
        if db.users.exists(user_id):
            user = db.users.get_or_create(user_id)
            if message in [subscription['search'] for subscription in user['subscriptions']]:
                for i in range(len(user['subscriptions'])):
                    if user['subscriptions'][i]['search'] == message:
                        self.subscription_details(user, user['subscriptions'][i])
                        break
            else:
                self.job_by_vacancies(user, message)
                Analytics.pageview(user_id, 'search_by_vacancies')
        else:
            db.users.get_or_create(user_id)
            return self.job_by_vacancies_or_subscription(user_id, message)

    def job_by_vacancies(self, user, vacancy):
        vacancies = self.search_by_vacancies(vacancy, 1)
        if vacancies:
            self.set_search_state(user, vacancy, 'vacancy', 1)
            results_count = self.search_by_vacancies_count(vacancy)
            self.send_results(user['user_id'], vacancies, vacancy, results_count)
            Analytics.pageview(user['user_id'], 'search_by_vacancies_{}'.format(vacancy.lower().replace(' ', '_')))
        else:
            self.set_search_state(user, None, None, None)
            message = strings['no_results']
            keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                              [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
            api_vk.send_message(user['user_id'], message, keyboard)
            Analytics.pageview(user['user_id'], 'no_results_for_{}'.format(vacancy.lower().replace(' ', '_')))

    def show_next_page(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, user['search'], user['search_type'], user['search_page'] + 1)
        results = self.get_search_results(user)
        if results:
            results_count = db.jobs.search_count(user['search_type'], user['search']) \
                if user['search_type'] != 'vacancy' else self.search_by_vacancies_count(user['search'])
            self.send_results(user_id, results, user['search'], results_count)
            Analytics.pageview(user_id, 'search_next_page')
        else:
            self.show_previous_page(user_id)

    def show_next_page_new(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, user['search'], user['search_type'], user['search_page'] + 1)
        results = self.get_new_search_results(user)
        if results:
            results_count = db.new_jobs.search_count(user['search_type'], user['search']) \
                if user['search_type'] != 'vacancy' else self.search_by_new_vacancies_count(user['search'])
            self.send_new_results(user_id, results, user['search'], results_count)
            Analytics.pageview(user_id, 'search_next_page')
        else:
            self.show_previous_page_new(user_id)

    def show_previous_page(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        if user['search_page'] - 1 >= 1:
            self.set_search_state(user, user['search'], user['search_type'], user['search_page'] - 1)
            results = self.get_search_results(user)
            results_count = db.jobs.search_count(user['search_type'], user['search']) \
                if user['search_type'] != 'vacancy' else self.search_by_vacancies_count(user['search'])
            self.send_results(user_id, results, user['search'], results_count)
        else:
            self.show_current_page(user_id)

    def show_previous_page_new(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        if user['search_page'] - 1 >= 1:
            self.set_search_state(user, user['search'], user['search_type'], user['search_page'] - 1)
            results = self.get_new_search_results(user)
            results_count = db.new_jobs.search_count(user['search_type'], user['search']) \
                if user['search_type'] != 'vacancy' else self.search_by_new_vacancies_count(user['search'])
            self.send_new_results(user_id, results, user['search'], results_count)
        else:
            self.show_current_page_new(user_id)

    def show_current_page(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, user['search'], user['search_type'], user['search_page'])
        results = self.get_search_results(user)
        results_count = db.jobs.search_count(user['search_type'], user['search']) \
            if user['search_type'] != 'vacancy' else self.search_by_vacancies_count(user['search'])
        self.send_results(user_id, results, user['search'], results_count)

    def show_current_page_new(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, user['search'], user['search_type'], user['search_page'])
        results = self.get_new_search_results(user)
        results_count = db.new_jobs.search_count(user['search_type'], user['search']) \
            if user['search_type'] != 'vacancy' else self.search_by_new_vacancies_count(user['search'])
        self.send_new_results(user_id, results, user['search'], results_count)

    def set_search_state(self, user, search, search_type, search_page):
        user['search'] = search
        user['search_type'] = search_type
        user['search_page'] = search_page
        db.users.set(user)

    def get_search_results(self, user):
        if user['search_type'] != 'vacancy':
            results = db.jobs.search(user['search_type'], user['search'], user['search_page'])
        else:
            results = self.search_by_vacancies(user['search'], user['search_page'])
        return results

    def get_new_search_results(self, user):
        if user['search_type'] != 'vacancy':
            results = db.new_jobs.search(user['search_type'], user['search'], user['search_page'])
        else:
            results = self.search_by_new_vacancies(user['search'], user['search_page'])
        return results

    def search_by_vacancies(self, vacancy, page):
        vacancies = []
        for row_vacancy in strings['vacancies']:
            if vacancy.lower() in row_vacancy.lower():
                vacancies.append(db.jobs.search('vacancy', row_vacancy, 1)[0])
        vacancies.reverse()
        return vacancies[(page-1)*5:page*5]

    def search_by_new_vacancies(self, vacancy, page):
        vacancies = []
        for row_vacancy in strings['new_vacancies']:
            if vacancy.lower() in row_vacancy.lower():
                vacancies.append(db.new_jobs.search('vacancy', row_vacancy, 1)[0])
        vacancies.reverse()
        return vacancies[(page-1)*5:page*5]

    def search_by_vacancies_count(self, vacancy):
        vacancies = []
        for row_vacancy in strings['vacancies']:
            if vacancy.lower() in row_vacancy.lower():
                vacancies.append(db.jobs.search('vacancy', row_vacancy, 1)[0])
        return len(vacancies)

    def search_by_new_vacancies_count(self, vacancy):
        vacancies = []
        for row_vacancy in strings['new_vacancies']:
            if vacancy.lower() in row_vacancy.lower():
                vacancies.append(db.new_jobs.search('vacancy', row_vacancy, 1)[0])
        return len(vacancies)

    def send_results(self, user_id, results, search, amount):
        message = strings['list_header'].format(search, amount)
        for job in results:
            message += strings['vacancy'].format(job['vacancy'],
                                                 job['salary'],
                                                 job['responsibilities'],
                                                 job['link'])
        keyboard = api_vk.create_keyboard([strings['btn_subscribe'], VkKeyboardColor.POSITIVE],
                                          [strings['btn_next'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)

    def send_new_results(self, user_id, results, search, amount):
        message = strings['new_vacancies_header'].format(search, amount)
        for job in results:
            message += strings['vacancy'].format(job['vacancy'],
                                                 job['salary'],
                                                 job['responsibilities'],
                                                 job['link'])
        keyboard = api_vk.create_keyboard([strings['btn_look_next'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)

    def check_subscription(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        subscription = {'search': user['search'], 'search_type': user['search_type']}
        if subscription in user['subscriptions']:
            self.subscription_exists(user)
        elif len(user['subscriptions']) >= 3:
            self.subscription_limit(user)
        else:
            self.set_subscription(user)

        Analytics.pageview(user['user_id'], 'subscribe_{}'.format(user['search'].lower().replace(' ', '_')))

    def set_subscription(self, user):
        subscription = {'search': user['search'] + '...', 'search_type': user['search_type']}
        user['subscriptions'].append(subscription)
        db.users.set(user)
        keyboard = api_vk.create_keyboard([strings['btn_continue'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        message = strings['subscription_is_set'].format(user['search'])
        api_vk.send_message(user['user_id'], message, keyboard)
        Analytics.event(user['user_id'], 'set_subscription')

    def subscription_exists(self, user):
        keyboard = api_vk.create_keyboard([strings['btn_continue'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        message = strings['subscription_exists']
        api_vk.send_message(user['user_id'], message, keyboard)
        Analytics.event(user['user_id'], 'subscription_exists')

    def subscription_limit(self, user):
        keyboard = api_vk.create_keyboard([strings['btn_continue'], VkKeyboardColor.DEFAULT],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        message = strings['subscription_limit']
        api_vk.send_message(user['user_id'], message, keyboard)
        Analytics.event(user['user_id'], 'subscription_limit')

    def manage_subscriptions(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        if not user['subscriptions']:
            self.you_have_no_subscriptions(user_id)
        else:
            self.choose_subscription_to_manage(user)

        Analytics.pageview(user['user_id'], 'manage_subscriptions')

    def you_have_no_subscriptions(self, user_id):
        message = strings['no_subscriptions']
        keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                          [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)

    def choose_subscription_to_manage(self, user):
        buttons = [[subscription['search'], VkKeyboardColor.DEFAULT] for subscription in user['subscriptions']]
        buttons.append([strings['btn_delete_all'], VkKeyboardColor.DEFAULT])
        buttons.append([strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        message = strings['choose_subscription']
        keyboard = api_vk.create_keyboard(*buttons)
        api_vk.send_message(user['user_id'], message, keyboard)

    def subscription_details(self, user, subscription):
        user['current_subscription'] = subscription
        db.users.set(user)
        message = strings['current_subscription'].format(subscription['search'][:-3])
        keyboard = api_vk.create_keyboard([strings['btn_show_vacancies'], VkKeyboardColor.POSITIVE],
                                          [strings['btn_delete'], VkKeyboardColor.NEGATIVE],
                                          [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user['user_id'], message, keyboard)

    def show_new_vacancies(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, user['current_subscription']['search'][:-3],
                              user['current_subscription']['search_type'], 1)
        results = self.get_new_search_results(user)
        results_count = db.new_jobs.search_count(user['search_type'], user['search']) \
            if user['search_type'] != 'vacancy' else self.search_by_new_vacancies_count(user['search'])
        if results:
            self.send_new_results(user_id, results, user['search'], results_count)
        else:
            message = strings['no_new_vacancies']
            keyboard = api_vk.create_keyboard([strings['btn_show_vacancies'], VkKeyboardColor.POSITIVE],
                                              [strings['btn_delete'], VkKeyboardColor.NEGATIVE],
                                              [strings['btn_main_menu'], VkKeyboardColor.PRIMARY])
            api_vk.send_message(user['user_id'], message, keyboard)

    def delete_subscription(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        user['subscriptions'].remove(user['current_subscription'])
        user['current_subscription'] = None
        db.users.set(user)
        message = strings['subscription_is_deleted']
        keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                          [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)
        Analytics.event(user['user_id'], 'delete_subscription')

    def delete_all_subscriptions(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        user['subscriptions'] = []
        user['current_subscription'] = None
        db.users.set(user)
        message = strings['subscriptions_are_deleted']
        keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                          [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)
        Analytics.event(user['user_id'], 'delete_all_subscriptions')

    def main_menu(self, user_id):
        api_vk.send_chat_action(user_id)
        user = db.users.get_or_create(user_id)
        self.set_search_state(user, None, None, None)
        user['current_subscription'] = None
        db.users.set(user)
        message = strings['main_menu']
        keyboard = api_vk.create_keyboard([strings['btn_find_job'], VkKeyboardColor.PRIMARY],
                                          [strings['btn_subscriptions'], VkKeyboardColor.PRIMARY])
        api_vk.send_message(user_id, message, keyboard)
        Analytics.pageview(user['user_id'], 'main_menu')

    def send_id(self, user_id):
        api_vk.send_message(user_id, str(user_id))

    def upload_new_vacancies(self, user_id):
        api_vk.send_message(user_id, strings['uploading'])
        updater.update_jobs()
        api_vk.send_message(user_id, strings['done'])

    def remove_vacancies(self, user_id, message):
        if not message.startswith('\n\n'):
            api_vk.send_message(user_id, strings['how_to_delete'])
            api_vk.send_message(user_id, strings['delete_template'])
        else:
            api_vk.send_message(user_id, strings['deleting'])
            vacancies_list = message[2:].split('\n')
            counter = 0
            for vacancy in vacancies_list:
                if vacancy in strings['vacancies']:
                    strings['vacancies'].remove(vacancy)
                    db.jobs.remove_vacancy(vacancy)
                    db.new_jobs.remove_vacancy(vacancy)
                    counter += 1
            api_vk.send_message(user_id, strings['done'])
            api_vk.send_message(user_id, strings['x_are_removed'].format(counter))
