## -*- coding: utf-8 -*-
from celery import Celery


token = '707921f08c1318d31321b8d9a4c62923794a88412c186710f1cede4fc5c34e1d8d28da2832a195b1ff502'
group_id = '103585114'

token_test = 'bff5ff5549d65c8b1921feeb6b707e5d0143f418750a7766b741af4e9251f83c27611f96a9a77e080e9f3'
group_id_test = '174631178'

MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DB = 'urfu_jobs_bot'

ga_url = 'https://www.google-analytics.com/batch'
protocol_version = 1
ga_terminal = 'UA-132350836-1'

celery = Celery()
celery.conf.update(
    broker_url='redis://localhost:6679/0',
    task_ignore_result=True,
    broker_transport_options={'visibility_timeout': 31536000}
)

JOBS_URL = 'https://времякарьеры.рф/forbot/'