## -*- coding: utf-8 -*-
import requests
from circuitbreaker import circuit

from bot.settings import ga_url, protocol_version, ga_terminal


class Analytics():
    @staticmethod
    def pageview(user_id, page, app=None):
        payload = {
            'v': protocol_version,
            'tid': ga_terminal,
            'cid': user_id,
            'an': app,

            't': 'pageview',
            'dp': page
        }
        requests.get(ga_url, params=payload)

    @staticmethod
    def event(user_id, action, app=None):
        payload = {
            'v': protocol_version,
            'tid': ga_terminal,
            'cid': user_id,
            'an': app,

            't': 'event',
            'ec': action,
            'ea': action,
        }
        requests.get(ga_url, params=payload)

