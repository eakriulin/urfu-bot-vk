## -*- coding: utf-8 -*-
import xlrd
import random
from bot.api import ApiVk
from bot.repository.db import MongoDB


db = MongoDB()
api_vk = ApiVk()


def insert_short_url(url):
    try:
        return 'Подробнее: {}'.format(api_vk.get_short_link(url))
    except:
        return 'Подробнее: {}'.format(url)


def get_description(text):
    formatted_text = '– ' + '\n– '.join(text.replace('\n', '').split('– ')[1:4])
    return formatted_text if formatted_text != '– ' else text


def run(filename, sheet_index):
    book = xlrd.open_workbook(filename)
    sheet = book.sheet_by_index(sheet_index)
    db.new_jobs.repo.remove({})
    print(sheet.nrows)
    print(range(sheet.nrows))
    for rx in random.sample(range(sheet.nrows), sheet.nrows):
        try:
            db.jobs.add(
                str(sheet.cell_value(rowx=rx, colx=0)),
                str(sheet.cell_value(rowx=rx, colx=1)).capitalize(),
                get_description(str(sheet.cell_value(rowx=rx, colx=2))),
                insert_short_url(str(sheet.cell_value(rowx=rx, colx=3))),
                str(sheet.cell_value(rowx=rx, colx=4)),
                str(sheet.cell_value(rowx=rx, colx=5)),
            )
            db.new_jobs.add(
                str(sheet.cell_value(rowx=rx, colx=0)),
                str(sheet.cell_value(rowx=rx, colx=1)).capitalize(),
                get_description(str(sheet.cell_value(rowx=rx, colx=2))),
                insert_short_url(str(sheet.cell_value(rowx=rx, colx=3))),
                str(sheet.cell_value(rowx=rx, colx=4)),
                str(sheet.cell_value(rowx=rx, colx=5)),
            )
        except Exception as err:
            print(err)
            print(str(sheet.cell_value(rowx=rx, colx=0)))


run('./bot/files/jobs5.xlsx', 0)
db.new_jobs.set_actual()
