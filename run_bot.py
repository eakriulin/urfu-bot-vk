## -*- coding: utf-8 -*-
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType

from bot.api import ApiVk
from bot.bot_urfu import BotUrfu
from bot.settings import group_id
#from bot.settings import group_id_test as group_id # testing stuff
from bot.strings import strings
from bot.analytics import Analytics

api_vk = ApiVk()
bot_urfu = BotUrfu()
longpoll = VkBotLongPoll(api_vk.session, group_id)


def handle_message(user_id, message):
    if message == strings['btn_find_job']:
        bot_urfu.choose_search_type(user_id)
    elif message == strings['btn_for_students']:
        bot_urfu.job_for_students(user_id)
    elif message == strings['btn_by_industries']:
        bot_urfu.show_industries(user_id)
    elif message in strings['industries']:
        bot_urfu.job_by_industries(user_id, message)
    elif message == strings['btn_next']:
        bot_urfu.show_next_page(user_id)
    elif message == strings['btn_main_menu']:
        bot_urfu.main_menu(user_id)
    elif message == strings['btn_subscribe']:
        bot_urfu.check_subscription(user_id)
    elif message == strings['btn_continue']:
        bot_urfu.show_current_page(user_id)
    elif message == strings['btn_subscriptions']:
        bot_urfu.manage_subscriptions(user_id)
    elif message == strings['btn_show_vacancies']:
        bot_urfu.show_new_vacancies(user_id)
    elif message == strings['btn_look_next']:
        bot_urfu.show_next_page_new(user_id)
    elif message == strings['btn_delete']:
        bot_urfu.delete_subscription(user_id)
    elif message == strings['btn_delete_all']:
        bot_urfu.delete_all_subscriptions(user_id)
    elif message == '/myid':
        bot_urfu.send_id(user_id)
    elif message == '/update':
        bot_urfu.upload_new_vacancies(user_id)
    elif message.startswith('/delete'):
        bot_urfu.remove_vacancies(user_id, message[7:]) # message without beginning part "/delete"
    else:
        bot_urfu.job_by_vacancies_or_subscription(user_id, message)

    Analytics.event(user_id, 'message')


def handle_start(user_id):
    bot_urfu.login(user_id)
    Analytics.event(user_id, 'message')


def main():
    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_ALLOW:
            user_id = event.obj.user_id
            handle_start(user_id)

        elif event.type == VkBotEventType.MESSAGE_NEW:
            message = event.obj.text
            user_id = event.obj.from_id
            handle_message(user_id, message)


if __name__ == '__main__':
    main()
